# Gitlab CI/CD Pipeline Example for Data Science with security

This demo allows you to show the features of modifying a notebook in a GitLab Merge Request to populate the Model Experiments in GitLab.  You can then show creating the model for production by tagging and putting in the id of the experiment to use for the settings

## Prep

1. Fork this project or clone it for your own use
2. Create a project access token and store it as a project or group level CI variable called MLFLOW_TRACKING_TOKEN and store the same token in a variable called REPO_TOKEN
3. Create another CI variable called MLFLOW_TRACKING_URI and store in it the value from. the Model Registry under the three-dot menu for MLFlow
4. Create a starter model in the model registry with the name breast_cancer_predictor and any version number and no artifacts

## Demo process

1. Create an issue to try a new grow_policy
2. Create an MR from the issue
4. Modify the training notebook in the WebIDE by changing the train_candidates.ipynb file to have "grow_policy": "depthwise" instead of "lossguide"
5. Commit the changes to the notebook
6. Observe the Model Experiments in GitLab being populated with the new run
7. Make note and copy the ID of the experiment with the best result
8. Merge the MR to move the code into the main branch
9. In the GitLab UI, tag the experiment to use for the production model and paste the is of the experiemnt chosen earlier
10. Observe the pipeline against main updating the model in the Model registry

## Ideas to improve

1. Use Changes rules in the CI config instead of having every change by a manual trigger of the training job
2. Build the docker container and use the GitLab container registry

